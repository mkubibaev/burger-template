import React, {Component, Fragment} from 'react';
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary";
import {addIngredient, removeIngredient} from "../../store/actions/BurgerBuilder";
import {connect} from "react-redux";

class BurgerBuilder extends Component {

    state = {
        purchasable: false,
        purchasing: false
    };

    isPurchasable = () => {
        const sum = Object.values(this.props.ingredients)
            .reduce((sum, el) => sum + el, 0);

        return sum > 0;
    };

    purchase = () => {
        this.setState({purchasing: true});
    };

    purchaseCancel = () => {
        this.setState({purchasing: false});
    };

    purchaseContinue = () => {
        this.props.history.push('/checkout');
    };

    render() {
        const disabledInfo = {...this.props.ingredients};

        for (const key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] === 0;
        }

        return (
            <Fragment>
                <Modal
                    show={this.state.purchasing}
                    close={this.purchaseCancel}
                >
                    <OrderSummary
                        ingredients={this.props.ingredients}
                        price={this.props.totalPrice}
                        purchaseCancel={this.purchaseCancel}
                        purchaseContinue={this.purchaseContinue}
                    />
                </Modal>
                <Burger ingredients={this.props.ingredients}/>
                <BuildControls
                    purchase={this.purchase}
                    price={this.props.totalPrice}
                    ingredientAdded={this.props.onIngredientAdded}
                    ingredientRemoved={this.props.onIngredientRemoved}
                    disabledInfo={disabledInfo}
                    purchasable={this.isPurchasable()}
                />
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    ingredients: state.bb.ingredients,
    totalPrice: state.bb.totalPrice
});

const mapDispatchToProps = dispatch => ({
    onIngredientAdded: ingName => dispatch(addIngredient(ingName)),
    onIngredientRemoved: ingName => dispatch(removeIngredient(ingName))
});


export default connect(mapStateToProps, mapDispatchToProps)(BurgerBuilder);
