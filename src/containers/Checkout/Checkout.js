import React, {Component, Fragment} from 'react';
import CheckoutSummary from "../../components/Order/CheckoutSummary";
import ContactData from "./ContactData/ContactData";
import {Route} from "react-router-dom";
import {connect} from "react-redux";

class Checkout extends Component {

    checkoutCancelled = () => {
        this.props.history.goBack();
    };

    checkoutContinued = () => {
        this.props.history.replace('/checkout/contact-data');
    };

    render() {
        return (
            <Fragment>
                <CheckoutSummary
                    ingredients={this.props.ingredients}
                    checkoutCancelled={this.checkoutCancelled}
                    checkoutContinued={this.checkoutContinued}
                />
                <Route
                    path={this.props.match.path + '/contact-data'}
                    component={ContactData}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    ingredients: state.bb.ingredients
});

export default connect(mapStateToProps)(Checkout);
