import React, {Component} from 'react';
import Button from "../../../components/UI/Button/Button";
import Spinner from "../../../components/UI/Spinner/Spinner";
import './ContactData.css';
import {connect} from "react-redux";
import {createOrder} from '../../../store/actions/order';

class ContactData extends Component {
    state = {
        name: '',
        email: '',
        street: '',
        postal: '',
    };

    valueChanged = event => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    orderHandler = event => {
        event.preventDefault();

        const orderData = {
            ingredients: this.props.ingredients,
            price: this.props.price,
            customer: {...this.state}
        };

        this.props.createOrder(orderData, this.props.history);
    };

    render() {
        let form = (
            <form onSubmit={this.orderHandler}>
                <input className="Input" type="text" name="name" placeholder="Your Name"
                       value={this.state.name} onChange={this.valueChanged}
                />
                <input className="Input" type="email" name="email" placeholder="Your Mail"
                       value={this.state.email} onChange={this.valueChanged}
                />
                <input className="Input" type="text" name="street" placeholder="Street"
                       value={this.state.street} onChange={this.valueChanged}
                />
                <input className="Input" type="text" name="postal" placeholder="Postal Code"
                       value={this.state.postal} onChange={this.valueChanged}
                />
                <Button btnType="Success">ORDER</Button>
            </form>
        );

        if (this.props.loading) {
            form = <Spinner/>;
        }

        return (
            <div className="ContactData">
                <h4>Enter your Contact Data</h4>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ingredients: state.bb.ingredients,
    price: state.bb.totalPrice,
    loading: state.order.loading
});

const mapDispatchToProps = dispatch => ({
    createOrder: (orderData, history) => dispatch(createOrder(orderData, history))
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactData);
