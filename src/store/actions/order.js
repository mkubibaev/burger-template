import axios from '../../axios-orders';
import {ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS} from "./actionTypes";
import { initIngredients } from './BurgerBuilder';

export const orderRequest = () => ({type: ORDER_REQUEST});
export const orderSuccess = () => ({type: ORDER_SUCCESS});
export const orderFailure = error => ({type: ORDER_FAILURE, error});

export const createOrder = (orderData, history) => {
    return dispatch => {
        dispatch(orderRequest());

        axios.post('orders.json', orderData).then(
            response => {
                dispatch(orderSuccess());
                dispatch(initIngredients());
                history.push('/');
            },
            error => dispatch(orderFailure(error))
        );
    }
};